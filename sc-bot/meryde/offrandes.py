import logging
from os import getenv
from discord import app_commands
from utils.getoffrande import get_offrande
from utils.gethumanreadabledate import get_human_readable_date
from reactionmenu import ViewMenu, ViewButton
from datetime import datetime, timedelta
from re import search
from multiprocessing import Pool
from asyncio import new_event_loop, set_event_loop

OUTDIR = getenv('OUT_DIR')
log = logging.getLogger('2')

def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)

async def get_offrande_task_async(single_date: datetime):
     offrande = await get_offrande(single_date)
     date = await get_human_readable_date(str(single_date.day), str(single_date.month))
     return f"\n🌿 **{offrande}** - *{date}*"

def get_offrande_task(single_date: datetime):
    loop = new_event_loop()
    set_event_loop(loop)
    result = loop.run_until_complete(get_offrande_task_async(single_date))
    loop.close()
    return result

@app_commands.describe(days="Le nombre de jours dont tu veux obtenir l'offrande (30 par défaut).")
@app_commands.rename(days='nombres_de_jour')
@app_commands.command(name = "meryde-offrandes", description = "Donne une liste des prochaines offrandes")
async def offrandes(interaction, days: int = 30):
    log.info(f"/meryde-offrandes has been called by { interaction.user.name} to know {days} days.")

    await interaction.response.defer(thinking = True)

    if not search(r"^[0-9]{1,3}$", str(days)) or days > 370 or days < 1:
        interaction.followup.send(f"{days} n'est pas une valeur valide, ce doit être un entier compris entre 1 et 370.")
        return None

    content = f"**MAO {interaction.user.mention}.** Voilà la liste des {days} prochaines offrandes.\n"

    now = datetime.now()
    start_date = now
    end_date = now + timedelta(days=days)

    pool = Pool(100)
    res = pool.map(get_offrande_task, list(daterange(start_date, end_date)))
    pool.close()
    pool.join()

    menu = ViewMenu(interaction, menu_type=ViewMenu.TypeText)
    menu.add_button(ViewButton.end_session())
    
    for offrande in res:
        if len(f"{content}{offrande}") < 1950:
            content += offrande
        else:
            menu.add_page(content=content)
            content = ""

    if menu.total_pages > 1:
        menu.add_button(ViewButton.back())
        menu.add_button(ViewButton.next())

    if content:
        menu.add_page(content=content)

    await menu.start()
