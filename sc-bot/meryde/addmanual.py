from os import path, getenv
from unidecode import unidecode
import logging
from discord import app_commands
from utils import newuser, removefromlist
OUTDIR = getenv('OUT_DIR')
log = logging.getLogger('2')

@app_commands.command(name = "meryde-add-manual", description = "Ajout manuel de Méryde")
@app_commands.describe(user_input_meryde_name="Le nom du méryde que tu souhaites ajouter à ta liste de mérydes effectués.")
@app_commands.rename(user_input_meryde_name='nom_du_méryde')
async def add_manual(interaction, user_input_meryde_name: str):
    log.info(f"/meryde-add-manual has been called by { interaction.user.name}.")
    user_merydes_file = f"{OUTDIR}/{interaction.user.name}/merydes.json"
    if not path.exists(user_merydes_file):
        await newuser.new_user(interaction.user.name, interaction)
        await interaction.response.send_message("Tu dois exécuter /meryde-end au moins une fois")
        return None
    meryde_name = unidecode(user_input_meryde_name).lower()
    await removefromlist.remove_from_list(interaction=interaction, meryde_name=meryde_name, username=interaction.user.name)
