from discord import app_commands
from utils import getmerydeoftheday, removefromlist
import logging
log = logging.getLogger('2')


@app_commands.command(name = "meryde-add-today", description = "Informe le bot que t'as fait le méryde du jour.")
async def add_today(interaction):
    log.info(f"/meryde-add-today has been called by { interaction.user.name}.")
    today_meryde_name = await getmerydeoftheday.get_meryde_of_the_day()
    await removefromlist.remove_from_list(interaction=interaction, username=interaction.user.name, meryde_name=today_meryde_name)
