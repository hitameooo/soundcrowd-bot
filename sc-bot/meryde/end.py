from os import getenv, fsencode, fsdecode, listdir, path

from numpy import e
from discord import app_commands
from utils import getmenu
from easyocr import Reader
from re import search, sub, compile, IGNORECASE
from unidecode import unidecode
import json
import logging
log = logging.getLogger('2')

OUTDIR = getenv('OUT_DIR')
APPDIR = getenv('APP_DIR')

@app_commands.command(name = "meryde-end", description = "HIT ME quand tu as fini d'upload tes screens de méryde avec /meryde-add-screen")
async def end(interaction):
    log.info(f"/meryde-end has been called by { interaction.user.name}.")

    user_merydes_file = f"{OUTDIR}/{interaction.user.name}/merydes.json"
    if path.exists(user_merydes_file):
        await interaction.response.send_message("Tu as déjà déclenché la moulinette magique, il n'est pas conseillé de la lancer une deuxième fois. Utilise /meryde-get si tu veux obtenir la liste de tes mérydes.")
        return None

    await interaction.response.defer(thinking = True)

    meryde_screens_dir_path = f"{OUTDIR}/{interaction.user.name}/pics"
    meryde_screens_dir = fsencode(meryde_screens_dir_path)

    reader = Reader(['fr'])
    ocr_results = []
    for file in listdir(meryde_screens_dir):
        filename = fsdecode(file)
        filepath = path.join(meryde_screens_dir_path, filename)
        ocr_results += reader.readtext(f"{filepath}")

    merydes_file = f"{APPDIR}/static/meryde_data/merydes.json"
    with open(merydes_file, 'r') as f:
      merydes = json.load(f)

    regexes = [ compile(r"[Oo]ffrande à"), compile(r"[Oo]ffrande"), compile(r"^ *à *$") ]

    detected_merydes = []
    detected_meryde_count = 0
    unknown = []
    unknown_count = 0
    for result in ocr_results:
        string_detected = result[1]

        for regex in regexes:
            string_detected = sub(regex, '', string_detected)
        
        string_detected = unidecode(string_detected)
        string_detected = string_detected.strip()
        string_detected = string_detected.lower()

        if string_detected in merydes:
            detected_merydes.append(string_detected.lower())
            detected_meryde_count += 1
            log.debug(f"Meryde found : {string_detected}.")

        elif string_detected == '':
            continue

        else:
            unknown.append(string_detected)
            unknown_count += 1
            log.info(f"Didn't recognize : {string_detected}")

    detected_merydes = list(set(detected_merydes))
    detected_merydes.sort()

    for meryde in detected_merydes:
        del merydes[meryde]

    with open(user_merydes_file, 'w') as f:
        f.write(json.dumps(merydes, sort_keys=True, indent=4, separators=(',', ': ')))

    if unknown_count != 0:
        message = "OOPS, il y a peut-être eu un soucis pendant la reconnaissance d'image."
        log.info(f"Erreur OCR : unknown {str(unknown)}")
        await interaction.followup.send(message)
    else:
        await getmenu.get_meryde_menu(interaction)
