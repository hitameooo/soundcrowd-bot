
from os import path, getenv
from utils import newuser
import logging
from discord import File, app_commands
OUTDIR = getenv('OUT_DIR')
APPDIR = getenv('APP_DIR')
log = logging.getLogger('2')


@app_commands.command(name = "meryde-help", description = "MEO ! Il te manque des mérydes ?")
async def help(interaction):
    meryde_help_message = open(f"{APPDIR}/static/txt/meryde_help","r").read()
    log.info(f"/meryde-help has been called by { interaction.user.name}.")
    userdir = f"{OUTDIR}/{interaction.user.name}"
    if not path.exists(userdir):
        await newuser.new_user(interaction.user.name, interaction)
    help_message = f"Meo copain { interaction.user.mention }{meryde_help_message}"

    await interaction.response.send_message(content=help_message, file=File(f"{APPDIR}/static/meryde_data/meryde_screen_example.png"))
