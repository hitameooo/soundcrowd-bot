from discord import app_commands
from utils import getmenu
import logging
log = logging.getLogger('2')


@app_commands.command(name = "meryde-get", description = "Récupère ta liste de mérydes manquants.")
async def get(interaction):
    log.info(f"/meryde-get has been called by { interaction.user.name}.")
    await getmenu.get_meryde_menu(interaction)
