from re import search
import json 
import logging
from datetime import datetime, timedelta
from os import getenv
from discord import app_commands
from utils import getmerydeoftheday, getoffrandeoftheday

OUTDIR = getenv('OUT_DIR')
log = logging.getLogger('2')

@app_commands.describe(reminder_hour="L'heure à laquelle tu souhaites être MP par le bot. Format attendu : HH:MM. Par exemple : 09:00.")
@app_commands.rename(reminder_hour='heure_souhaitée')
@app_commands.command(name = "meryde-reminder", description = "Configure le bot pour qu'il te MP à l'heure de ton choix les jours où il te manque un Méryde.")
async def reminder(interaction, reminder_hour: str):
    log.info(f"/meryde-reminder has been called by { interaction.user.name} to set his reminder at the following time : { reminder_hour }.")
    if not search('^[0-2]?[0-9]:[0-5][0-9]$', reminder_hour):
        error_message = "L'heure que tu as donnée est dans un format incorrecte. Format attendu : HH:MM. Par exemple : 09:00."
        await interaction.response.send_message(error_message)
        return None

    date_format = '%Y-%m-%dT%H:%M:%S'
    present = datetime.now()
    meryde_reminder = {}
    meryde_reminder["hour"] = reminder_hour
    hour = meryde_reminder["hour"].split(':')[0]
    minute = meryde_reminder["hour"].split(':')[1]
    reminder_date = present.replace(hour=int(hour), minute=int(minute))

    serialized_reminder_date = datetime.strftime(reminder_date, date_format)
    meryde_reminder["last_run_timestamp"] = serialized_reminder_date

    meryde_reminder_file_path = f'{ OUTDIR }/{ interaction.user.name }/meryde_reminder.json'
    open(meryde_reminder_file_path, "w").write(
        json.dumps(meryde_reminder, sort_keys=True, indent=4, separators=(',', ': '))
    )
    guild_message = f"C'est setup ! Le bot t'enverra un MP à {reminder_hour} les jours où il te manque un méryde !"
    await interaction.response.send_message(guild_message)

    direct_message = f"✌ MEO { interaction.user.name }. ✌\n**Je t'enverrai un MP à {reminder_hour} les jours où il te manque un méryde.**"

    merydeoftheday = await getmerydeoftheday.get_meryde_of_the_day()

    with open(f"{OUTDIR}/{interaction.user.name}/merydes.json", 'r') as f:
        user_merydes = json.load(f)

    if merydeoftheday in user_merydes:
        merydeoftheday = merydeoftheday.capitalize()
        offrandeoftheday = await getoffrandeoftheday.get_offrande_of_the_day()
        direct_message += f"\n\n**HA !** et je crois que tu n'as pas la quête d'offrande d'aujourd'hui à **{merydeoftheday}** : apporte **{ offrandeoftheday }** au temple Almanax !"
    await interaction.user.send(content=direct_message)
