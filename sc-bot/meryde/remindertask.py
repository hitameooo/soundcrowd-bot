import json
import logging
from os import path, getenv
from datetime import datetime, timedelta
from utils import getmerydeoftheday, log, getoffrandeoftheday
from discord.ext import tasks

OUTDIR = getenv('OUT_DIR')
log = logging.getLogger('2')

@tasks.loop(minutes=5)
async def reminder_task(guild):
    log.info(f"Meryde reminder task running.")
    meryde_of_the_day = await getmerydeoftheday.get_meryde_of_the_day()
    offrande_of_the_day = await getoffrandeoftheday.get_offrande_of_the_day()
    for member in guild.members:
        meryde_reminder_file_path = f"{OUTDIR}/{member.name}/meryde_reminder.json"
        if not path.exists(meryde_reminder_file_path):
            continue

        with open(f"{OUTDIR}/{member.name}/merydes.json", 'r') as f:
            user_merydes = json.load(f)  
            if not meryde_of_the_day in user_merydes:
                log.debug(f"{member.name} has no {meryde_of_the_day} in his list : he has already completed the quest.")
                continue

        date_format = '%Y-%m-%dT%H:%M:%S'

        reminder_file = open(meryde_reminder_file_path, 'r')
        reminder_data = json.load(reminder_file)
        reminder_file.close()

        present = datetime.now()
        last_run_timestamp = datetime.strptime(reminder_data['last_run_timestamp'], date_format)

        if last_run_timestamp + timedelta(days=1) > present:
            continue

        hour = reminder_data["hour"].split(':')[0]
        minute = reminder_data["hour"].split(':')[1]
        today_run_timestamp = present.replace(hour=int(hour), minute=int(minute))
        
        if today_run_timestamp > present:
            continue

        meryde_of_the_day = meryde_of_the_day.capitalize()
        log.info(f"Meryde reminder task : sending DM to { member.name }.")
        await member.send(content=f"🐈 Meo bonjour { member.name } ! 🐈 Tu n'as pas la quête d'offrande d'aujourd'hui !\nC'est le méryde **{ meryde_of_the_day }**. Apporte **{ offrande_of_the_day }** au temple Almanax !") 

        serialized_today_run_timestamp = datetime.strftime(today_run_timestamp, date_format)
        reminder_data["last_run_timestamp"] = serialized_today_run_timestamp

        open(meryde_reminder_file_path, "w").write(
            json.dumps(reminder_data, sort_keys=True, indent=4, separators=(',', ': '))
        )
