import logging
from os import path, getenv
from random import choices
from string import digits, ascii_uppercase
from discord import app_commands, Attachment
from utils import newuser

OUTDIR = getenv('OUT_DIR')
log = logging.getLogger('2')

@app_commands.command(name = "meryde-add-screen", description = "Ajout initial de Méryde, par screens")
async def add_screen(interaction, file: Attachment):
    log.info(f"/meryde-add-screen has been called by { interaction.user.name}.")
    userdir = f"{OUTDIR}/{interaction.user.name}"
    if not path.exists(userdir):
        await newuser.new_user(interaction.user.name, interaction)
    random_filename = ''.join(choices(ascii_uppercase + digits, k=20))
    filename = f"{ OUTDIR }/{ interaction.user.name }/pics/{ random_filename }"
    await file.save(fp=filename.format(file.filename))
    await interaction.response.send_message("Got this")
