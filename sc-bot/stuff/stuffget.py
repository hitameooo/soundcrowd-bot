from os import getenv
from discord import app_commands
from json import load
import logging
log = logging.getLogger('2')
OUTDIR = getenv('OUT_DIR')

from utils.getstuffnames import get_stuff_names

@app_commands.autocomplete(stuff_name=get_stuff_names)
@app_commands.command(name = "stuff-get", description = "Don le stuff")
async def stuff_get(interaction, stuff_name: str):
    log.info(f"/stuff-get has been called by { interaction.user.name} with arg {stuff_name}.")
    stuffs = {}
    default_category = "200"
    with open(f"{OUTDIR}/stuffs.json", 'r') as f:
        stuffs = load(f)
    if stuff_name in stuffs[default_category]:
        url = f"https://www.dofusbook.net/fr/equipement/{stuffs[default_category][stuff_name]['id']}/objets"
        msg = f"Stuff 🪖 **{stuff_name}** 🪖 dispo ici : {url}"
    else:
        msg = f"🪖 Je n'ai pas trouvé le stuff avec le nom : **{stuff_name}**"
    await interaction.response.send_message(msg)
