from os import getenv
import logging
from discord import app_commands
APPDIR = getenv('APP_DIR')
log = logging.getLogger('2')

@app_commands.command(name = "stuff-help", description = "Tu cherches un stuff ?")
async def stuff_help(interaction):
    stuff_help_message = open(f"{APPDIR}/static/txt/stuff_help","r").read()
    log.info(f"/stuff-help has been called by { interaction.user.name}.")
    help_message = f"{stuff_help_message}"
    await interaction.response.send_message(content=help_message)
