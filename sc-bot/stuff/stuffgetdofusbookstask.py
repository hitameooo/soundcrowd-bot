from os import getenv
from re import search, compile
from json import dumps

import logging
log = logging.getLogger('2')
OUTDIR = getenv('OUT_DIR')

from discord.ext import tasks

from selenium import webdriver 
from bs4 import BeautifulSoup
from time import sleep

from selenium.webdriver.firefox.options import Options
options = Options()
options.headless = True
driver = webdriver.Firefox(options=options,
                           service_log_path=f"{OUTDIR}/gecko.log")

db_base_url = "https://www.dofusbook.net"
db_profile_uri = "/fr/membre/"

async def get_html_soup(url: str):
    log.info(f"Stuff task - Crawling {url}")
    driver.get(url)
    sleep(3)
    html = driver.page_source.encode("utf-8")
    soup = BeautifulSoup(html, "lxml")
    return soup

async def get_last_page_number(url: str) -> int:
    soup = await get_html_soup(f"{url}&page=9999")
    uls = soup.findAll("ul", {"class": "pagination"})
    last_page_number = 1
    page_number_re = compile(r"^[0-9]+$")
    if uls: 
        for li in uls[0].findAll("li"):
            page_number = li.find("button").text
            if search(page_number_re, page_number):
                if int(page_number) > last_page_number:
                    last_page_number = int(page_number)

    return last_page_number

#async def get_stuff_task(db_profile_id: str = "675237-wakka"):
@tasks.loop(minutes=27)
async def stuff_get_dofusbooks_task():
    log.info(f"Stuff task - Starting")
    db_profile_id = "911375-calm"
    categories = {
        "200" : "1703422",
        "199" : "1703421",
        "Passage" : "1703406",
        "Prospection" : "1703424",
        "Sagesse" : "1703423"
    }

    stuffs = {}
    for category_name, category_id in categories.items():
        url = f"{db_base_url}{db_profile_uri}{db_profile_id}/equipements?folder={category_id}"
        last_page_number = await get_last_page_number(url)
        log.debug(f"Stuff task - Last page number is {last_page_number}")
        stuffs[category_name] = {}
        stuffs[category_name]["url"] = url
        for page_number in range(1, last_page_number+1):
            page_url = f"{url}&page={page_number}"
            soup = await get_html_soup(page_url)

            stuff_divs = soup.findAll("div", {"class": "col-md-12 px-3 pb-6"})

            for stuff_div in stuff_divs:
                stuff_name_div = stuff_div.find("div", {"class": "title"})
                if not stuff_name_div:
                    continue
                stuff_name = stuff_name_div.text
                stuff_link_a = stuff_div.find("a", {"class": "link"})
                if stuff_link_a:
                    stuff_link = stuff_link_a['href']
                    # stuff_link is int the form of : /fr/equipement/13877248-eau-do-crit/objets
                    stuff_id = stuff_link.split('/')[3]
                try:
                    log.info(stuff_name_div)
                    log.info(category_name)
                    stuffs[category_name][stuff_name] = {}
                    stuffs[category_name][stuff_name]["id"] = stuff_id
                except NameError:
                    ...

    with open(f"{OUTDIR}/stuffs.json", "w") as f:
        f.write(dumps(stuffs))
        log.info(f"Stuffs have been updated at {OUTDIR}/stuffs.json")
   
    log.info(f"Stuff task - Ending")
