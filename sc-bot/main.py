# client.py

from os import getenv, mkdir, path
from dotenv import load_dotenv
from discord import Intents, app_commands, Client
from utils import log
import logging
log.logger()
logger = logging.getLogger('discord')

load_dotenv()
TOKEN  = getenv('DISCORD_TOKEN')
GUILD  = getenv('DISCORD_GUILD')
OUTDIR = getenv('OUT_DIR')
if not path.exists(OUTDIR):
  mkdir(OUTDIR)

intents = Intents.all()
client = Client(intents=intents)
tree = app_commands.CommandTree(client)

from meryde import help, add_manual, add_today, add_screen, end, get, reminder, reminder_task, offrandes
from soundcrowd import soundcrowd_quote, soundcrowd_redir, unreal_cat, soundcrowd_help
from sonj import sonj_score_get, sonj_score_set
#from stuff import stuff_get, stuff_get_dofusbooks_task, stuff_help

@client.event
async def on_ready():

    commands = [help, add_manual, add_today, add_screen, end, get, offrandes, reminder, soundcrowd_quote, soundcrowd_redir, unreal_cat, sonj_score_get, sonj_score_set, soundcrowd_help]
    for command in commands:
        tree.add_command(command)

    await tree.sync()

    for guild in client.guilds:
        if guild.name == GUILD:
            logger.info(
                f'{client.user} is connected to the following guild:\n'
                f'{guild.name}(id: {guild.id})'
            )
            
            await tree.sync(guild=guild)
            reminder_task.start(guild)
            #stuff_get_dofusbooks_task.start()
            break

client.run(TOKEN)
