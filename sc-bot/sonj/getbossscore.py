from discord import app_commands, Interaction
import logging
from utils.getbossembed import get_boss_embed
from utils.getbossnames import get_boss_names

log = logging.getLogger('2')

@app_commands.autocomplete(boss_name=get_boss_names)
@app_commands.command(name = "sonj-score-get", description = "Récupère le score associé à un boss donné lors d'un ajout en songes")
async def sonj_score_get(interaction: Interaction, boss_name:str):
    log.info(f"/sonj-score-get has been called by { interaction.user.name}.")

    boss_embed, file = get_boss_embed(boss_name)
    await interaction.response.send_message(file=file, embed=boss_embed)
