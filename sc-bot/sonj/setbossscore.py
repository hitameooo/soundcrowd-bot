from os import getenv
from discord import app_commands, Interaction
from json import load, dumps
import logging
from utils.getbossembed import get_boss_embed
from utils.getbossnames import get_boss_names

log = logging.getLogger('2')

@app_commands.autocomplete(boss_name=get_boss_names)
@app_commands.describe(boss_name="Boss pour lequel ajouter un nouveau score.",
                       score="Score à ajouter pour ce boss.")
@app_commands.rename(boss_name="nom_du_boss", score = "score_à_ajouter")
@app_commands.command(name = "sonj-score-set", description = "Ajoute un nouveau score d'ajout de boss sonj")
async def sonj_score_set(interaction: Interaction, boss_name:str, score:int):
    log.info(f"/sonj-score-set has been called by { interaction.user.name}.")

    OUTDIR = getenv('OUT_DIR')
    boss_file = f"{OUTDIR}/sonj/boss_scores.json"
    boss_scores = []

    with open(boss_file, 'r') as f:
        boss_scores = load(f)

    boss = next(item for item in boss_scores if item["name"].lower() in boss_name.lower())
    boss_index = next(i for i, item in enumerate(boss_scores) if item["name"].lower() in boss_name.lower())

    boss["scores"].append(score)
    boss_scores[boss_index] = boss
    with open(boss_file, 'w') as f:
        f.write(dumps(boss_scores, sort_keys=True, indent=4, separators=(',', ': ')))

    boss_embed, file = get_boss_embed(boss_name)
    await interaction.response.send_message(file=file, embed=boss_embed)

