import random
import json
from os import getenv
from discord import app_commands
import logging
from requests import post
APPDIR = getenv('APP_DIR')
log = logging.getLogger('2')

@app_commands.command(name = "unreal-cat", description = "Un chat qui n'existe pas.")
async def unreal_cat (interaction):
    log.info(f"/unreal-cat has been called by { interaction.user.name}.")
    await interaction.response.defer(thinking = True)
    OPENAI_API_KEY = "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"
    headers = {
        'Content-Type': 'application/json',
        'Authorization': f'Bearer {OPENAI_API_KEY}',
    }
    
    json_data = {
        'prompt': 'a cat in a unusual situation',
        'n': 1,
        'size': '1024x1024',
    }
    
    response = post('https://api.openai.com/v1/images/generations', headers=headers, json=json_data)
    log.info(response)
    response_json = json.loads(response.content.decode(('utf-8')))
    log.info(response_json)
    unreal_cat_url = response_json["data"][0]["url"]

    await interaction.followup.send(unreal_cat_url)
