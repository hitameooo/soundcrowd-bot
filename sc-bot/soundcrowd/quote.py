
import random
from os import getenv
from discord import app_commands
import logging
APPDIR = getenv('APP_DIR')
log = logging.getLogger('2')

@app_commands.command(name = "soundcrowd-quote", description = "Random SoundCrowd quote")
async def soundcrowd_quote(interaction):
    log.info(f"/soundcrowd-quote has been called by { interaction.user.name}.")
    quotes_file = open(f"{APPDIR}/static/txt/sc_quotes")
    line = next(quotes_file)
    for num, aline in enumerate(quotes_file, 2):
        if random.randrange(num):
            continue
        line = aline
    quotes_file.close()
    await interaction.response.send_message(line)
