from os import getenv, listdir, fsencode, fsdecode
from discord import app_commands, Interaction, File
from typing import List
import logging
APPDIR = getenv('APP_DIR')
log = logging.getLogger('2')

async def get_redir(
    interaction: Interaction,
    current: str,
) -> List[app_commands.Choice[str]]:
    redirs = []
    redirs_dir = fsencode(f"{APPDIR}/static/redir")
    for file in listdir(redirs_dir):
        filename = fsdecode(file)
        redirs.append(filename)
    return [
        app_commands.Choice(name=redir, value=redir)
        for redir in redirs if current.lower() in redir.lower()
    ]

@app_commands.autocomplete(redir=get_redir)
@app_commands.command(name = "soundcrowd-redir", description = "Les bonnes redirs de la famille SoundCrowd")
async def soundcrowd_redir(interaction: Interaction, redir:str):
    await interaction.response.send_message(content='MAO', file=File(f"{APPDIR}/static/redir/{redir}"))
    log.info(f"/soundcrowd-redir has been called by { interaction.user.name}.")

