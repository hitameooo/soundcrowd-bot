
from os import path, getenv
from utils import newuser
import logging
from discord import app_commands
OUTDIR = getenv('OUT_DIR')
APPDIR = getenv('APP_DIR')
log = logging.getLogger('2')


@app_commands.command(name = "soundcrowd-help", description = "MEO ! Petit manuel des commandes du bot.")
async def soundcrowd_help(interaction):
    soundcrowd_message = open(f"{APPDIR}/static/txt/soundcrowd_help","r").read()
    log.info(f"/soundcrowd-help has been called by { interaction.user.name}.")
    userdir = f"{OUTDIR}/{interaction.user.name}"
    if not path.exists(userdir):
        await newuser.new_user(interaction.user.name, interaction)
    help_message = f"Meo copain { interaction.user.mention }{soundcrowd_message}"

    await interaction.response.send_message(content=help_message)
