from bs4 import BeautifulSoup
from requests import get
from utils.getoffrande import get_offrande
from datetime import datetime

async def get_offrande_of_the_day() -> str: 
    today_offrande = await get_offrande(datetime.now())
    return today_offrande
