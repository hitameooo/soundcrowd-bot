from discord import app_commands, Interaction
from typing import List
from json import load
from os import getenv

async def get_boss_names(
    interaction: Interaction,
    current: str,
) -> List[app_commands.Choice[str]]:
    OUTDIR = getenv('OUT_DIR')
    boss_scores = []
    boss_file = f"{OUTDIR}/sonj/boss_scores.json"
    with open(boss_file, 'r') as f:
        boss_scores = load(f)

    boss_names = []
    for boss in boss_scores:
        boss_names.append(boss["name"])

    return [
        app_commands.Choice(name=boss_name, value=boss_name)
        for boss_name in boss_names if current.lower() in boss_name.lower()
        ][:20]

