from os import getenv
from discord import Embed, Colour, File
from json import load

def get_boss_embed(boss_name: str) -> tuple[Embed, File]:
    APPDIR = getenv('APP_DIR')
    OUTDIR = getenv('OUT_DIR')
    boss_file = f"{OUTDIR}/sonj/boss_scores.json"
    boss_scores = []
    with open(boss_file, 'r') as f:
        boss_scores = load(f)

    boss = next(item for item in boss_scores if item["name"].lower() in boss_name.lower())
    
    if len(boss["scores"]) > 0:
        average_score = sum(boss["scores"]) / len(boss["scores"])
        last_50_scores = boss["scores"][-50:]
        description = f"**Score d'ajout moyen : {average_score}**\n\nListe des scores d'ajout enregistrés : {', '.join(str(e) for e in last_50_scores)}"
    else:
        description = "Aucun score enregistré pour ce boss pour le moment."

    file = File(f'{APPDIR}/static/mobs/pics/{boss["name"]}.png', filename=f"boss.png")
    boss_embed = Embed(colour = Colour.yellow(),
                     title = f" <:sonj:930941825244606484> {boss['name']}",
                     description = description)

    boss_embed.set_thumbnail(url=f"attachment://boss.png")

    return boss_embed, file

