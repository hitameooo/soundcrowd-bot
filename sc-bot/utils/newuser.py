from os import mkdir, path, getenv
import logging
log = logging.getLogger('2')
OUTDIR = getenv('OUT_DIR')


async def new_user( username, interaction):
    user_dir = f"{OUTDIR}/{username}"
    if not path.exists(f"{user_dir}"):
        mkdir(f"{user_dir}")
    if not path.exists(f"{user_dir}/pics"):
        mkdir(f"{user_dir}/pics")
    log.info(f"New user : { username }. Directory { user_dir } has been created.")


