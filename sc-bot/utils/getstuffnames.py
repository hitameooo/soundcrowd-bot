from discord import app_commands, Interaction
from typing import List
from json import load
from os import getenv
import logging

async def get_stuff_names(
    interaction: Interaction,
    current: str
) -> List[app_commands.Choice[str]]:
    
    OUTDIR = getenv('OUT_DIR')
    stuffs = {}
    stuffs_file = f"{OUTDIR}/stuffs.json"
    with open(stuffs_file, 'r') as f:
        stuffs = load(f)

    stuff_names = []
    for stuff_name in stuffs:
        stuff_names.append(stuff_name)

    matching_stuffs = []
    for stuff_name in stuff_names:
        next_stuff = False
        for term in current.lower().split(' '):
            if not term in stuff_name.lower():
                next_stuff = True
                continue
        if next_stuff:
            continue
        matching_stuffs.append(stuff_name)
    #matching_stuffs = set(matching_stuffs)
    return [
        app_commands.Choice(name=stuff_name, value=stuff_name)
        for stuff_name in matching_stuffs
        ][:20]

