import json
from os import getenv, path
from re import compile, search
from datetime import datetime
from discord import Embed
from reactionmenu import ViewMenu, ViewButton
from utils import gethumanreadabledate
import logging
log = logging.getLogger('2')
OUTDIR = getenv('OUT_DIR')

async def get_meryde_menu(interaction):
    with open(f"{OUTDIR}/{interaction.user.name}/merydes.json", 'r') as f:
        results = json.load(f)

    results = {k: v for k, v in sorted(results.items(), key=lambda item: datetime(2000, int(item[1]["month"]), int(item[1]["day"])))}  

    menu = ViewMenu(interaction, menu_type=ViewMenu.TypeEmbed)
    
    description = "🔮 **Meryde Menu** 🔮\n\n"

    description += f"""Tu as actuellement { 373 - len(results)} mérydes, sur un total de 373.
    Il te manque {len(results)} mérydes copain {interaction.user.mention}
    Tu peux voir la liste de tes mérydes manquants en regardant les pages suivantes de ce message."""

    meryde_reminder_file_path = f"{OUTDIR}/{interaction.user.name}/meryde_reminder.json"
    if path.exists(meryde_reminder_file_path):
        meryde_reminder = {}
        with open(meryde_reminder_file_path, 'r') as f:
            meryde_reminder = json.load(f)

        description += f"\n⌛ *Ton reminder est défini à **{meryde_reminder['hour']}***" 
    embed = Embed(description=description)
    menu.add_page(embed)

    goto_first_button = ViewButton(emoji='⏮', custom_id=ViewButton.ID_GO_TO_FIRST_PAGE)
    prev_button = ViewButton(emoji='⏪', custom_id=ViewButton.ID_PREVIOUS_PAGE)
    next_button = ViewButton(emoji='⏩', custom_id=ViewButton.ID_NEXT_PAGE)
    goto_last_button = ViewButton(emoji='⏭', custom_id=ViewButton.ID_GO_TO_LAST_PAGE)
    menu.add_button(goto_first_button)
    menu.add_button(prev_button)
    menu.add_button(next_button)
    menu.add_button(goto_last_button)

    increment = 1
    description = ''
    for meryde in results:
        if "name" in results[meryde] :
            date = results[meryde]['name']
        else:
            date = await gethumanreadabledate.get_human_readable_date(day=results[meryde]["day"], month=results[meryde]["month"])

        description += f"**{meryde.capitalize()}** - {date}\n"
        #description += f"{increment}. **{meryde.capitalize()}** - {date}\n"
        if increment % 10 == 0:
            embed = Embed(description=f'{description}')
            menu.add_page(embed)
            description = ''
        increment += 1

    if description != '':
        embed = Embed(description=f'{description}')
        menu.add_page(embed)

    await menu.start()
