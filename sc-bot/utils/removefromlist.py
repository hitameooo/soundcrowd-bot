
import json
from os import getenv
import logging
log = logging.getLogger('2')
OUTDIR = getenv('OUT_DIR')

async def remove_from_list(interaction, username: str, meryde_name: str):
    userdir = f"{OUTDIR}/{username}"
    user_merydes_file_path = f"{userdir}/merydes.json"
    user_merydes_file = open(user_merydes_file_path)
    user_merydes = json.load(user_merydes_file)
    user_merydes_file.close()

    if meryde_name in user_merydes:
        del user_merydes[meryde_name]
        message = f"Done ! Le méryde { meryde_name } a été enlevé de ta liste."

        open(user_merydes_file_path, "w").write(
            json.dumps(user_merydes, sort_keys=True, indent=4, separators=(',', ': '))
        )

    else:
        message = f"Wat ? Je n'ai pas trouvé le méryde { meryde_name } dans ta liste ! Tu l'as déjà fait nan ?"

    await interaction.response.send_message(content=message)
