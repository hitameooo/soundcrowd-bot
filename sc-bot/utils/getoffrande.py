from bs4 import BeautifulSoup
from requests import get
from re import compile, search
from datetime import datetime
import logging
log = logging.getLogger('2')

async def get_offrande(date: datetime) -> str:

    year = f"{date.year}"
    month = '{:02d}'.format(date.month)
    day = '{:02d}'.format(date.day)
    date_string = f"{year}-{month}-{day}"
    krozmoz_html_page = get(f"https://www.krosmoz.com/fr/almanax/{date_string}")
    soup = BeautifulSoup(krozmoz_html_page.text, "lxml")
    offrande_of_the_day_div = soup.find("div", { "id": "achievement_dofus"}) 
    offrande_of_the_day_p = offrande_of_the_day_div.find("p", {"class": "fleft"})
    offrande_of_the_day = offrande_of_the_day_p.text.split("Récupérer ")[1].split(" et rapporter")[0]
    return offrande_of_the_day
