from bs4 import BeautifulSoup
from unidecode import unidecode
from requests import get
from os import getenv
import logging
from time import sleep

OUTDIR = getenv('OUT_DIR')
log = logging.getLogger('2')

async def get_meryde_of_the_day() -> str: 
    today_meryde_name_raw = await get_meryde_of_the_day_raw()
    log.info(f"raw name : {today_meryde_name_raw}")
    if today_meryde_name_raw:
        today_meryde_name = unidecode(today_meryde_name_raw).lower().strip()
    else:
        today_meryde_name = ''
    return today_meryde_name

async def get_meryde_of_the_day_raw() -> str:
    request_ok = False
    max_attempts = 10
    attempts = 0
    while request_ok is False:
        try:
            krozmoz_html_page = get('https://www.krosmoz.com/fr/almanax')
            soup = BeautifulSoup(krozmoz_html_page.text, "lxml")
            today_meryde_name_raw = soup.find("div", {"id": "almanax_boss_desc"}).span.text
            request_ok = True
        except:
            log.info(f"Can't retrieve the name of the meryde of the day on attempt {attempts}/{max_attempts}. Raw soup is : {soup}.")
            request_ok = False
            attempts += 1
            if attempts >= max_attempts:
                log.info("10 retries failed, exiting the task.")
                today_meryde_name_raw = ''
                break
            else:
                log.info("Sleeping 1 min and retrying.")
                sleep(60) 
    return today_meryde_name_raw 
