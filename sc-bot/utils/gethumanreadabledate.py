async def get_human_readable_date(day: str, month: str) -> str:
    if month == '1':
        month_name = 'Janvier'
    elif month == '2':
        month_name = 'Février'
    elif month == '3':
        month_name = 'Mars'
    elif month == '4':
        month_name = 'Avril'
    elif month == '5':
        month_name = 'Mai'
    elif month == '6':
        month_name = 'Juin'
    elif month == '7':
        month_name = 'Juillet'
    elif month == '8':
        month_name = 'Août'
    elif month == '9':
        month_name = 'Septembre'
    elif month == '10':
        month_name = 'Octobre'
    elif month == '11':
        month_name = 'Novembre'
    elif month == '12':
        month_name = 'Décembre'
    if day == '1':
        day = '1er'
    return f"{day} {month_name}"
