FROM python:3.10.10-bullseye

RUN groupadd -g 2789 sc-bot \
    && useradd -m -u 2789 -g 2789 -s /bin/bash sc-bot

COPY . /app

WORKDIR /app/sc-bot

RUN pip install -r requirements.txt \
    && apt update -y \
    && apt install -y libnss3 libnspr4 firefox-esr # selenium requirements

USER sc-bot

CMD [ "python", "main.py" ]
